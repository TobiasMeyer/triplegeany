<?php
// read json-file with ontology desciption
$json = json_decode(file_get_contents('data/vraw.json'), true);
//

// define arrays
$prefixes = array();
$pattern = array();
$replacement = array();

// fill arrays
$i = 0;
while (isset($json['prefix'][$i])) {
	array_push($prefixes, $json['prefix'][$i]['short']);
	array_push($pattern, '/' . $json['prefix'][$i]['short'] . '/');
	array_push($replacement, urlencode('<' . $json['prefix'][$i]['long']));
	$i++;
}

// read querystring from user input
$string = $_POST['test'];

// seperate string to array of words
$array = explode(" ", $string);

// replace prefixes with according urls
foreach ($array as &$value) {

	foreach ($prefixes as $prefix) {
		if (contains_substr($value, $prefix)) {
			$value = preg_replace($pattern, $replacement, $value);
			$value .= "%3E";
		}

	}

}

// store and return manipulated string
$string = implode("%20", $array);
echo $string;


// check if $str is part of the $mainStr
function contains_substr($mainStr, $str, $loc = false) {
	if ($loc === false)
		return (strpos($mainStr, $str) !== false);
	if (strlen($mainStr) < strlen($str))
		return false;
	if (($loc + strlen($str)) > strlen($mainStr))
		return false;
	return (strcmp(substr($mainStr, $loc, strlen($str)), $str) == 0);
}
?>