/**
 * @author Svantje
 */

// Ajax Http Request
/*var myAjax = ajaxObject();
myAjax.open('get', 'http://http://localhost:8080/openrdf-sesame/repositories/1/statements', true);
myAjax.setRequestHeader("Accept", "application/rdf+json");
myAjax.send();
myAjax.onreadystatechange = show;

function show(){
	var content = myAjax.responseText;
	alert(content);
}

function ajaxObject() {
	if (window.XMLHttpRequest) {
		myAjax = new XMLHttpRequest();
	} else {
		myAjax = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return myAjax;
}
*/

// ***** Beginn Dateiupload ******
// Funktion gibt Dateinamen, Dateigröße und Dateityp der ausgewählten Datei zurück und setzt die Progressbar auf 0 zurück.
function fileChange() {
	var fileList = document.getElementById("fileA").files;
	var file = fileList[0];
	if (!file)
		return;
	document.getElementById("fileName").innerHTML = 'Dateiname: ' + file.name;
	document.getElementById("fileSize").innerHTML = 'Dateigröße: ' + file.size + ' B';
	document.getElementById("fileType").innerHTML = 'Dateitype: ' + file.type;
	document.getElementById("progress").value = 0;
	document.getElementById("prozent").innerHTML = "0%";

}

function uploadFile() {
	//Wieder unser File Objekt
	var file = document.getElementById("fileA").files[0];
	//FormData Objekt erzeugen
	var formData = new FormData();
	//XMLHttpRequest Objekt erzeugen
	client = new XMLHttpRequest();
	var prog = document.getElementById("progress");
	if (!file)
		return;
	prog.value = 0;
	prog.max = 100;
	//Fügt dem formData Objekt unser File Objekt hinzu
	formData.append("datei", file);
	client.onerror = function(e) {
		alert("onError");
	};

	client.onload = function(e) {
		document.getElementById("prozent").innerHTML = "100%";
		prog.value = prog.max;
	};

	client.upload.onprogress = function(e) {
		var p = Math.round(100 / e.total * e.loaded);
		document.getElementById("progress").value = p;
		document.getElementById("prozent").innerHTML = p + "%";
	};

	client.onabort = function(e) {
		alert("Upload abgebrochen");
	};

	client.open("POST", "upload.php");
	client.send(formData);
}

function uploadAbort() {
	if ( client instanceof XMLHttpRequest)
		//Bricht die aktuelle Übertragung ab
		client.abort();
}

// ***** Ende Dateiupload ***

function dateiauswahl(evt) {
	// FileList object
	var files = evt.target.files;

	//Deklarierung eines Array Objekts mit Namen "output"Speicherung von Eigenschaften
	var output = [];
	//Zählschleife, bei jedem durchgang den Namen, Typ und die Dateigröße der ausgewählten Dateien zum Array hinzufügen
	for (var i = 0, f; f = files[i]; i++) {
		output.push('<li><strong>', f.name, '</strong> (', f.type || 'n/a', ') - ', f.size, ' bytes</li>');
	}
	//Auf das Ausgabefeld zugreifen, unsortierte Liste erstellen und das oben erstellte Array auslesen lassen
	document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
}

//Falls neue Eingabe, neuer Aufruf der Auswahlfunktion
//document.getElementById('files').addEventListener('change', dateiauswahl, false);

// JSON
function jsonTest() {
	var text = '[{"name":"peter"},{"name":"claus"},{"name":"paul"}]';
	var json = JSON.parse(text);
	for (var i = 0; i <= 2; i++) {
		document.write(json[i].name + "<br>");
	}

}

